###############################################
#---------------------------------------------#
# Data prep and subsetting for Boldness       # 
#---------------------------------------------#
###############################################



#-------------------------------------------------#
# Load packages and support files
source(file = "support/HighstatLibV10.R")  
source(file = "support/MCMCSupportHighstatV4.R")
library(nlme)
library(ggplot2)
library(lattice)
library(plyr)
#-------------------------------------------------#



#------------------------------------------------#
#Loading Prepared Final Data

fdata<-readRDS(file="data/final_pers_data.Rda")
str(fdata)
#------------------------------------------------#



#----------------------------------------------------------------------------------#
#Housekeeping
names(fdata)
#  Response variable:                          Main predictor:     Covariates:          Random effects:
#  bold_latency_to_return (range from 1,2700)  bold_age            batch                juv_id
#                                                                  cohort               mom_id
#                                                                  bold_body_temp       tub
#                                                                  sex
#                                                                  BCI

table(fdata$juv_id) #same number of repeats each juv = 12
table(fdata$mom_id) #varies from 12-48
table(fdata$tub)    #varies from 24 (social pair) to 12 (isolated)
#----------------------------------------------------------------------------------#



#----------------------------------------------------------------------------------#
# Data preparation and subsetting

#boldness assay repeated 3 times within each experiment

colSums(is.na(fdata))

#subsetting only data needed for bold analysis
b_dat<-subset(fdata, select=c(id,trait_rep,juv_id,tub,treat_sp,treat_ds,mom_id,batch,cohort,bold_body_temp,sex,BCI,bold_age,log_bold))
str(b_dat)

colSums(is.na(b_dat)) #yes there are NAs
                      #sex = 12 NAs (from B0310 that did not sex), and 9 missing bold values from video issues
b_dat2 <- na.exclude(b_dat) #removing NAs
colSums(is.na(b_dat2)) #no more NAs
table(b_dat2$trait_rep) #A,B,C represented

table(b_dat2$juv_id) #need to removed juveniles that have less than 12 samples for 
                     #B0001, B0020, B0025, B0102, B0115
                     #B0125, B0130, B1022, B1031

b_dat3<-b_dat2[
    b_dat2$juv_id != "B0001" &
    b_dat2$juv_id != "B0020" &
    b_dat2$juv_id != "B0025" &
    b_dat2$juv_id != "B0102" &
    b_dat2$juv_id != "B0115" &
    b_dat2$juv_id != "B0125" &
    b_dat2$juv_id != "B0130" &
    b_dat2$juv_id != "B1022" &  
    b_dat2$juv_id != "B1031",] 

table(b_dat3$juv_id) #removed, all juveniles now have N=12 obs

table(b_dat3$mom_id) #need to remove the moms that have 0 observations
b_dat3$mom_id2 <- droplevels(b_dat3$mom_id)
table(b_dat3$mom_id2)  #use this variable (mom_id2); 0s have been removed
b_dat4<-subset(b_dat3, select = -mom_id)
head(b_dat4)

table(b_dat4$tub) #remove any tubs with zero values 
b_dat4$tub2 <- droplevels(b_dat4$tub)
table(b_dat4$tub2)
b_dat4<-subset(b_dat4, select = -tub)
head(b_dat4)


#------------------------------------------------------#
#Saving Boldness Data
saveRDS(b_dat4, file="data/bold_data.Rda")
#------------------------------------------------------#

#when you need to load the data:
b_dat<-readRDS(file="data/bold_data.Rda")


#Sample Sizes
length(b_dat4$log_bold) #672
length(unique(b_dat4$juv_id)) #56
length(unique(b_dat4$mom_id2)) #31
length(unique(b_dat4$tub2)) #44