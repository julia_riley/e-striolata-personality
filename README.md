This is the information file for the code used in the paper **"Early social environment influences the behaviour of a family-living lizard"**.



Any comments or questions, please contact Julia Riley: julia.riley87@gmail.com




The following files are provided in the 'data' folder:

1. Agro Score Relationships.csv - aggression scores & attributes of skinks
2. Difference Ag Score Based on Age.csv - aggression score & age differences
3. Growth Data.csv - skink morphometric data
4. personality_data_NArm - final behavioural trait data for analyses
5. Social Cat Repeatibility - final adjusted repeatibility esimates from each social experience for plotting


The following files are provided in the 'script' folder & are for the following analyses steps:

*(1) Data exploration and preparation*

1. Data Prep_Overall Pers Data.R - first organisation of data
2. Data Prep_Exploration.R - organisation of data for exploration assay
3. Data Prep_Boldness.R - organisation of data for boldness assay
4. Data Prep_Social.R - organisation of data for sociability assay
5. Data Prep_Aggression.R - organisation of data for aggression assay


*(2) Analysis of Skink Aggression Scores and Attributes*

1. A_Aggression Scores & Trends.R


*(3) Analysis of Skink Morphometrics (SVL and tail length)*

1. B_SVL & TL vs. Social Cat.R


*(4) Analysis of Skink Behavioural Traits*

1. C_Exploration Social Cat Models.R
2. C_Boldness Social Cat Models.R
3. C_SocialA social cat models.R
4. C_Agro social cat models.R
5. C_Behavioural Syndrome_multivariate.R


*Figures*

1. Fig1_SVL and TL vs. social category.R
2. Fig2_Multipanel Plot.R
3. Fig3_social cat repeatibility.R


These additional files are within the main repository:

1. Analysis.Rproj - the Rstudio project for this analyses
2. .gitignore - the ignore file